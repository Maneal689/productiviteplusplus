function genRndTextStrat(strat) {
    var rngStrat = Math.floor(Math.random() * strat.length);
    return strat[rngStrat];
}

function msToStr(msCount) {
    let sec = parseInt(msCount / 1000);
    let min;

    min = parseInt(sec / 60);
    sec -= min * 60;
    return (min < 10 ? '0' + min : min) + ':' + (sec < 10 ? '0' + sec : sec);
}

function Timer(fullTime = 25 * 60 * 1000) {
    this.fullTime = fullTime;
    this.lastTick = null;
    this.passedTime = 0;
    this.stop = true;

    this.switch = function() {
        if (this.stop == true) {
            //Bouton start
            this.lastTick = new Date().getTime();
            this.stop = false;
        } else {
            //Bouton pause
            this.stop = true;
        }
    };

    this.reset = function(fullTime = this.fullTime) {
        this.fullTime = fullTime;
        this.lastTick = null;
        this.passedTime = 0;
        this.stop = true;
    };

    this.update = function() {
        let currentTime = new Date().getTime();
        if (!this.stop) {
            //Si l'horloge tourne
            this.passedTime += currentTime - this.lastTick;
        }
        this.lastTick = currentTime;
    };
}

$(document).ready(waitJson);

function waitJson() {
    $.getJSON('sentences.json', function(data) {
        runWindow(data.sentences);
    });
}

function runWindow(strat) {
    const stratText = $('#oblic-text');
    const clockText = $('#clock');
    const startBtn = $('#start');
    const resetBtn = $('#reset');
    const clockTabs = $('.clock-tab');
    //Init oblique strategy sentence
    stratText.html(genRndTextStrat(strat));
    let timers = [];
    let selectedTimer = 0;
    let clockProcessId;
    timers.push(new Timer());
    timers.push(new Timer(5 * 60 * 1000));
    timers.push(new Timer(10 * 60 * 1000));
    let nextClock = {
        0: 1,
        1: 0,
        2: 0,
    };

    for (let i = 0; i < clockTabs.length; i++) {
        clockTabs[i].onclick = function() {
            timers.forEach(timer => timer.reset());
            selectedTimer = i;
            clockTabs.removeClass('selected');
            clockTabs[i].classList.add('selected');
            window.clearInterval(clockProcessId);
            updateClock(timers[selectedTimer], clockText, startBtn);
        };
    }

    //Start update process
    //Init the randomize oblique strategy sentence
    $('#oblic-strat-tile').click(function() {
        stratText.html(genRndTextStrat(strat));
    });

    startBtn.click(() => {
        timers[selectedTimer].switch();
        if (timers[selectedTimer].stop) {
            startBtn.html('<i class="fas fa-play"></i>');
            window.clearInterval(clockProcessId);
        } else {
            clockProcessId = window.setInterval(() => {
                updateClock(timers[selectedTimer], clockText, startBtn);
            }, 250);
            startBtn.html('<i class="fas fa-pause"></i>');
        }
    });

    resetBtn.click(() => {
        timers[selectedTimer].reset();
        startBtn.html('<i class="fas fa-play"></i>');
        updateClock(timers[selectedTimer], clockText, startBtn);
        window.clearInterval(clockProcessId);
    });
}

function updateClock(clock, node, startBtn) {
    if (clock.stop) startBtn.html('<i class="fas fa-play"></i>');
    else startBtn.html('<i class="fas fa-pause"></i>');
    clock.update();
    node.html(msToStr(clock.fullTime - clock.passedTime));
}
